input = open('./Day_9/input.txt', 'r')
lines = input.readlines()

currentHeadPosition = [0,0]
previousHeadPosition = [0,0]
tailPositions = [[0,0]]

for line in lines:
    line = line.replace('\n', '')

    parts = line.split(' ')
    direction = parts[0]
    distance = parts[1]

    distanceLeft = int(distance)

    if (direction == 'R'):
        colModifier = 1
        rowModifier = 0
        
    if (direction == 'L'):
        colModifier = -1
        rowModifier = 0

    elif (direction == 'U'):
        colModifier = 0
        rowModifier = 1

    elif (direction == 'D'):
        colModifier = 0
        rowModifier = -1

    if (colModifier != 0 or rowModifier != 0):
        while (distanceLeft > 0):
            previousHeadPosition = currentHeadPosition
            currentHeadPosition = [currentHeadPosition[0]+colModifier, currentHeadPosition[1]+rowModifier]

            if ((abs(tailPositions[-1][0]-currentHeadPosition[0]) > 1 or abs(tailPositions[-1][1]-currentHeadPosition[1]) > 1)):
                tailPositions.append(previousHeadPosition)

            distanceLeft -= 1


print(len(list(set(map(tuple,tailPositions)))))