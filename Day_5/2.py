input = open('./Day_5/input.txt', 'r')
lines = input.readlines()

stackEnd = 0
moves = []
stackLines = []
numOfStacks = 0
stacks = []

for line in lines:

    if (line == '\n'):
        #print('end of stack setup')
        stackEnd = 1
    else:

        if (not stackEnd):
            #print(line.replace('\n', ''))
            stackCount = line.count('[')

            if (stackCount > 0):
                stackLines.append(line)

            numOfStacks = numOfStacks if stackCount == 0 else stackCount
        else:
            moves.append(line.strip())

stackLines.reverse()
stackCounter = 0
firstTime = 1
for stackLine in stackLines:
    #print(stackLine.replace('\n', ''))

    stackLine = stackLine.replace('    ', ' [ ]').strip()
    #print(stackLine)
    stackLine = stackLine.replace('] [', '][')
    stackItems = stackLine.split('][')

    while stackCounter < numOfStacks:
        if (firstTime == 1):
            stacks.append([])

        #print(stackCounter)
        stackItem = stackItems[stackCounter].replace('[', '').replace(']', '')
        if (stackItem != ' '):
            stacks[stackCounter].append(stackItem)
        stackCounter += 1

    firstTime = 0
    stackCounter = 0



for move in moves:
    numToMove = 0
    stackFrom = 0
    stackTo = 0

    things = move.split(' ')
    numToMove = int(things[1])
    stackFrom = int(things[3])-1
    stackTo = int(things[5])-1

    popped = stacks[stackFrom][-numToMove:]
    del stacks[stackFrom][len(stacks[stackFrom]) - numToMove:]
    popped.reverse()
    while numToMove > 0:
        stacks[stackTo].append(popped.pop())
        numToMove -= 1


print('------')
print(''.join([last for *_, last in stacks]))