input = open('./Day_6/input.txt', 'r')
lines = input.readlines()

charIndex = 0
currentFourteen = []


for char in lines[0]:
    currentFourteen.append(char)
    charIndex += 1

    if (len(currentFourteen) < 14):
        continue
    elif (len(currentFourteen) > 14):
        currentFourteen.pop(0)
    
    if (len(set(currentFourteen)) == 14):
        print(charIndex)
        break
    