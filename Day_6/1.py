input = open('./Day_6/input.txt', 'r')
lines = input.readlines()

charIndex = 0
currentFour = []


for char in lines[0]:
    currentFour.append(char)
    charIndex += 1

    if (len(currentFour) < 4):
        continue
    elif (len(currentFour) > 4):
        currentFour.pop(0)
    
    if (len(set(currentFour)) == 4):
        print(charIndex)
        break
    