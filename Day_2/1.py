input = open('./Day_2/input.txt', 'r')
lines = input.readlines()

# A for Rock     = 1 = X
# B for Paper    = 2 = Y
# C for Scissors = 3 = Z

# Outcome
# Lose = 0 
# Draw = 3
# Win = 6

rock = 1
paper = 2
scissors = 3

lose = 0
draw = 3
win = 6

score = 0

for line in lines:
    line = line.strip()
    print('-' + line + '-')

    if (line.startswith('A')):
        # Rock
        if (line.endswith('X')):
            # Rock
            print('Rock vs Rock = Draw')
            score += (rock + draw)
        elif (line.endswith('Y')):
            # Paper
            print('Rock vs Paper = Win')
            score += (paper + win)
        elif (line.endswith('Z')):
            # Scissors
            print('Rock vs Scissors = Lose')
            score += (scissors + lose)
    elif (line.startswith('B')):
        # Paper
        if (line.endswith('X')):
            # Rock
            print('Paper vs Rock = Lose')
            score += (rock + lose)
        elif (line.endswith('Y')):
            # Paper
            print('Paper vs Paper = Draw')
            score += (paper + draw)
        elif (line.endswith('Z')):
            # Scissors
            print('Paper vs Scissors = Win')
            score += (scissors + win)
    elif (line.startswith('C')):
        # Scissors
        if (line.endswith('X')):
            # Rock
            print('Scissors vs Rock = Win')
            score += (rock + win)
        elif (line.endswith('Y')):
            # Paper
            print('Scissors vs Paper = Lose')
            score += (paper + lose)
        elif (line.endswith('Z')):
            # Scissors
            print('Scissors vs Scissors = Draw')
            score += (scissors + draw)

print(score)