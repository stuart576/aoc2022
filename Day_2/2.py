input = open('./Day_2/input.txt', 'r')
lines = input.readlines()

# A for Rock     = 1
# B for Paper    = 2
# C for Scissors = 3

# Outcome
# Lose = 0 = X
# Draw = 3 = Y
# Win = 6  = Z

rock = 1
paper = 2
scissors = 3

lose = 0
draw = 3
win = 6

score = 0

for line in lines:
    line = line.strip()
    print('-' + line + '-')

    if (line.startswith('A')):
        # Rock
        if (line.endswith('X')):
            # Lose
            print('Rock vs Scissors = Lose')
            score += (scissors + lose)
        elif (line.endswith('Y')):
            # Draw
            print('Rock vs Rock = Draw')
            score += (rock + draw)
        elif (line.endswith('Z')):
            # Win
            print('Rock vs Paper = Win')
            score += (paper + win)
    elif (line.startswith('B')):
        # Paper
        if (line.endswith('X')):
            # Lose
            print('Paper vs Rock = Lose')
            score += (rock + lose)
        elif (line.endswith('Y')):
            # Draw
            print('Paper vs Paper = Draw')
            score += (paper + draw)
        elif (line.endswith('Z')):
            # Win
            print('Paper vs Scissors = Win')
            score += (scissors + win)
    elif (line.startswith('C')):
        # Scissors
        if (line.endswith('X')):
            # Lose
            print('Scissors vs Paper = Lose')
            score += (paper + lose)
        elif (line.endswith('Y')):
            # Draw
            print('Scissors vs Scissors = Draw')
            score += (scissors + draw)
        elif (line.endswith('Z')):
            # Win
            print('Scissors vs Rock = Win')
            score += (rock + win)

print(score)