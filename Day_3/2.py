input = open('./Day_3/input.txt', 'r')
lines = input.readlines()

itemTypes = []

currentGroup = []
counter = 0

for line in lines:
    line = line.strip()

    currentGroup.append(line)

    if (counter < 2):
        counter += 1
    else:
        # Got 3 backpacks, Find the common type
        # print(currentGroup)

        # Find common item type
        commonItemType = list(set(currentGroup[0]) & set(currentGroup[1]) & set(currentGroup[2]))[0]
        # print(commonItemType)

        modifier = 96 if ord(commonItemType) > 96 else 38
        charNum = ord(commonItemType) - modifier
        itemTypes.append(charNum)

        # Reset for next group
        currentGroup = []
        counter = 0

print(itemTypes)

print(sum(itemTypes))