input = open('./Day_3/input.txt', 'r')
lines = input.readlines()

# 16 (p), 38 (L), 42 (P), 22 (v), 20 (t), and 19 (s);

itemTypes = []

for line in lines:
    line = line.strip()

    stringLength = len(line)

    compartment1 = line[:stringLength//2]
    compartment2 = line[stringLength//2:]

    # print(line)
    commonChar = list(set(compartment1).intersection(compartment2))[0]

    # print(commonChar)

    modifier = 96 if ord(commonChar) > 96 else 38
    charNum = ord(commonChar) - modifier
    itemTypes.append(charNum)

# print(itemTypes)

print(sum(itemTypes))