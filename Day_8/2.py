input = open('./Day_8/input.txt', 'r')
lines = input.readlines()

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

grid = []
lineNum = 0

for line in lines:
    line = line.replace('\n', '')

    grid.append([])

    for char in line:
        grid[lineNum].append(char)

    lineNum += 1


rows = len(grid)
cols = len(grid[0])

numTreesVisibleFromEdge = 0

stringGrid = ''

bestScenicScore = 0

for rowNum in range(rows):
    for colNum in range(cols):
        scenicScoreLeft = 0
        scenicScoreRight = 0
        scenicScoreTop = 0
        scenicScoreBottom = 0

        # top and bottom edge
        if (rowNum == 0 or rowNum == rows-1):
            numTreesVisibleFromEdge += 1
            stringGrid += bcolors.WARNING + grid[rowNum][colNum] + bcolors.ENDC
        else:
            # left and right edge
            if (colNum == 0 or colNum == cols-1):
                numTreesVisibleFromEdge += 1
                stringGrid += bcolors.WARNING + grid[rowNum][colNum] + bcolors.ENDC
            else:
                # Look up
                treeHeight = grid[rowNum][colNum]
                currentRow = rowNum-1
                treeVisibleFromEdge = True

                while (treeVisibleFromEdge and currentRow >= 0):
                    topNeighbour = grid[currentRow][colNum]
                    if (topNeighbour >= treeHeight):
                        treeVisibleFromEdge = False
                    else:
                        currentRow -= 1
                        
                    scenicScoreTop += 1

                # Look down
                if (rowNum < rows-1):
                    currentRow = rowNum+1
                    treeVisibleFromEdge = True

                    while (treeVisibleFromEdge and currentRow < rows):
                        bottomNeighbour = grid[currentRow][colNum]
                        if (bottomNeighbour >= treeHeight):
                            treeVisibleFromEdge = False
                        else:
                            currentRow += 1
                        
                        scenicScoreBottom += 1

                # Look right
                if (colNum < cols-1):
                    currentCol = colNum+1
                    treeVisibleFromEdge = True

                    while (treeVisibleFromEdge and currentCol < cols):
                        rightNeighbour = grid[rowNum][currentCol]
                        if (rightNeighbour >= treeHeight):
                            treeVisibleFromEdge = False
                        else:
                            currentCol += 1
                            
                        scenicScoreRight += 1

                # Look left
                if (colNum > 0):
                    currentCol = colNum-1
                    treeVisibleFromEdge = True

                    while (treeVisibleFromEdge and currentCol >= 0):
                        leftNeighbour = grid[rowNum][currentCol]
                        if (leftNeighbour >= treeHeight):
                            treeVisibleFromEdge = False
                        else:
                            currentCol -= 1
                            
                        scenicScoreLeft += 1

                currentScenicScore = scenicScoreTop * scenicScoreBottom * scenicScoreLeft * scenicScoreRight
                if (currentScenicScore > bestScenicScore):
                    bestScenicScore = currentScenicScore
                
                if (treeVisibleFromEdge):
                    numTreesVisibleFromEdge += 1
                    stringGrid += bcolors.WARNING + grid[rowNum][colNum] + ' ' + bcolors.ENDC
                    continue

                stringGrid += grid[rowNum][colNum]

        stringGrid += ' '

    stringGrid += '\n'

print(numTreesVisibleFromEdge)
print(stringGrid)

print(bestScenicScore)