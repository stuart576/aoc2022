input = open('./Day_4/input.txt', 'r')
lines = input.readlines()

for line in lines:
    line = line.strip()

fullOverlaps = 0
partialOverlaps = 0

for line in lines:
    line = line.strip()

    assignments = line.split(',')
    firstElf = assignments[0]
    secondElf = assignments[1]

    firstElfStartEnd = firstElf.split('-')
    secondElfStartEnd = secondElf.split('-')

    firstElfStart = int(firstElfStartEnd[0])
    firstElfEnd = int(firstElfStartEnd[1])
    secondElfStart = int(secondElfStartEnd[0])
    secondElfEnd = int(secondElfStartEnd[1])

    if (firstElfStart <= secondElfStart and firstElfEnd >= secondElfEnd):
        fullOverlaps += 1
        print(assignments)
    elif (secondElfStart <= firstElfStart and secondElfEnd >= firstElfEnd):
        fullOverlaps += 1
        print(assignments)
    elif (firstElfEnd >= secondElfStart and firstElfStart <= secondElfEnd):
        partialOverlaps += 1
        print(assignments)
    elif (secondElfEnd >= firstElfStart and secondElfStart <= firstElfEnd):
        partialOverlaps += 1
        print(assignments)



print(fullOverlaps + partialOverlaps)
