input = open('./Day_7/input.txt', 'r')
lines = input.readlines()

currentDirectory = ''
directories = {}
currentPath = []
parents = {}
sizes = {}

for line in lines:
    line = line.replace('\n', '')

    if (line.startswith('$ cd ..')):
        currentPath.pop()

    elif (line.startswith('$ cd')):
        folder = line.replace('$ cd ', '')
        currentPath.append(folder)

        if '/'.join(currentPath) not in directories:
            directories['/'.join(currentPath)] = []
            
        if '/'.join(currentPath) not in sizes:
            sizes['/'.join(currentPath)] = 0

    elif (line.startswith('dir')):
        folder = line.replace('dir ', '')

        if '/'.join(currentPath) not in parents:
            parents['/'.join(currentPath)] = []
        
        parents['/'.join(currentPath)].append('/'.join(currentPath) + '/' + folder)

        
        if '/'.join(currentPath) not in sizes:
            sizes['/'.join(currentPath) + '/' + folder] = 0

    elif (not line.startswith('$')):
        directories['/'.join(currentPath)].append(line)

        if '/'.join(currentPath) not in sizes:
            sizes['/'.join(currentPath)] = 0

        sizes['/'.join(currentPath)] += int(line.split(' ')[0])

# print(directories)
# print(parents)
# print(sizes)

def recursiveSum(parent):
    currentSize = sizes[parent]

    for child in parents[parent]:
        if (child in parents):
            currentSize += recursiveSum(child)
        else:
            currentSize += sizes[child]
        
    sizes[parent] = currentSize

    return currentSize

recursiveSum('/')

max100000kfolders = [i for i in sizes if sizes[i] <= 100000]
# print(max100000kfolders)

print(sum([sizes[i] for i in max100000kfolders]))