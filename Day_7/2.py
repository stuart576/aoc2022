input = open('./Day_7/input.txt', 'r')
lines = input.readlines()

# total disk space  = 70000000
# need unused space = 30000000

currentDirectory = ''
directories = {}
currentPath = []
parents = {}
sizes = {}

for line in lines:
    line = line.replace('\n', '')

    if (line.startswith('$ cd ..')):
        currentPath.pop()

    elif (line.startswith('$ cd')):
        folder = line.replace('$ cd ', '')
        currentPath.append(folder)

        if '/'.join(currentPath) not in directories:
            directories['/'.join(currentPath)] = []
            
        if '/'.join(currentPath) not in sizes:
            sizes['/'.join(currentPath)] = 0

    elif (line.startswith('dir')):
        folder = line.replace('dir ', '')

        if '/'.join(currentPath) not in parents:
            parents['/'.join(currentPath)] = []
        
        parents['/'.join(currentPath)].append('/'.join(currentPath) + '/' + folder)

        
        if '/'.join(currentPath) not in sizes:
            sizes['/'.join(currentPath) + '/' + folder] = 0

    elif (not line.startswith('$')):
        directories['/'.join(currentPath)].append(line)

        if '/'.join(currentPath) not in sizes:
            sizes['/'.join(currentPath)] = 0

        sizes['/'.join(currentPath)] += int(line.split(' ')[0])

# print(directories)
# print(parents)
# print(sizes)

def recursiveSum(parent):
    currentSize = sizes[parent]

    for child in parents[parent]:
        if (child in parents):
            currentSize += recursiveSum(child)
        else:
            currentSize += sizes[child]
        
    sizes[parent] = currentSize

    return currentSize

recursiveSum('/')

# print(dict(sorted(sizes.items(), key=lambda kv: kv[1])))

totalSpaceUsed = sizes['/']
totalSpace = 70000000
spaceNeeded = 30000000

spaceFree = totalSpace - totalSpaceUsed
spaceToFind = spaceNeeded - spaceFree

print("Total Disk Space: {0}, Total Space Used: {1}, Free Space: {2}, SpaceToFind: {3}".format(totalSpace, totalSpaceUsed, spaceFree, spaceToFind))

for key in dict(sorted(sizes.items(), key=lambda kv: kv[1])):
    if sizes[key] >= spaceToFind:
        print(sizes[key])
        break;