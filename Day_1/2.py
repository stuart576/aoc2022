input = open('./Day_1/input.txt', 'r')
lines = input.readlines()

elves = []
elves.append(0)
elfIndex = 0
  
count = 0
for line in lines:
    if (line != '\n'):
        elves[elfIndex] += int(line)
    else:
        elfIndex += 1
        elves.append(0)

print(max(elves))