input = open('./Day_1/input.txt', 'r')
lines = input.readlines()

elves = []
elves.append(0)
elfIndex = 0
  
count = 0
for line in lines:
    if (line != '\n'):
        elves[elfIndex] += int(line)
    else:
        elfIndex += 1
        elves.append(0)

topElf = max(elves)
elvesList = list(elves)
elvesList.sort()
secondElf = elvesList[-2]
thirdElf = elvesList[-3]

print(topElf+secondElf+thirdElf)